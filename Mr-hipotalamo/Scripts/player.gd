extends CharacterBody2D

var accel = 100
var speed = 250

var axis : Vector2

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _physics_process(delta):
	player_movement(delta)

func get_axis() -> Vector2:
	axis.x = int(Input.is_action_pressed("Right")) - int(Input.is_action_pressed("Left"))
	axis.y = int(Input.is_action_pressed("Down")) - int(Input.is_action_pressed("Up"))
	return axis

func player_movement(delta):
	var direction = Vector2(get_axis().x, get_axis().y)
	direction = direction.normalized()
	
	velocity = velocity.lerp(direction * speed, accel*delta)
	
	move_and_slide()
	
	
