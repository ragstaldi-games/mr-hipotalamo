extends TouchScreenButton

enum state{
	HAPPY,
	NORMAL,
	EXHAUSTED
}

var has_shirt = true
var has_pants = true
var sweating = false

var temperature = 37

var happy_billy = [load("res://Assets/Billy_Happy.png"), load("res://Assets/Billy_Happy_shirtless.png"), load("res://Assets/Billy_Happy_nude.png")]
var normal_billy = [load("res://Assets/Billy_Normal.png"), load("res://Assets/Billy_Normal_sweat.png"), load("res://Assets/Billy_Normal_shirtless.png"), load("res://Assets/Billy_Normal_nude.png")]
var sad_billy = [load("res://Assets/Billy_Exhausted.png"), load("res://Assets/Billy_Exhausted_sweat.png"), load("res://Assets/Billy_Exhausted_shirtless.png"), load("res://Assets/Billy_Exhausted_nude.png")]
var open_billy = load("res://Assets/Billy_Butchered.png")

var billy_state : state

var past_second = -1
var dead = false

# Called when the node enters the scene tree for the first time.
func _ready():
	billy_state = state.NORMAL


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$"../Temperature".text = "Temperature: " + str(int(temperature)) + "°C"
	
	if dead:
		$"../AnimationPlayer".stop()
	
	if Global.temperature > 30 && Global.temperature < 35:		
		if sweating:
			modulate = Color(Color.WHITE, 255)
			if has_pants and has_shirt:
				temperature -= delta / 8
			if has_pants and !has_shirt:
				temperature -= delta / 4
			else:
				temperature -= delta / 2
		elif $"../Body".rapid_heart_rate:
			modulate = Color(Color.RED, 65)
			if has_pants and has_shirt:
				temperature -= delta / 7
			elif has_pants and !has_shirt:
				temperature -= delta / 6
			else:
				temperature -= delta / 5
			if past_second != Global.minute:
				past_second = Global.minute		
				dead = heart_attack()
				
		elif has_pants and has_shirt:
			modulate = Color(Color.WHITE, 255)
			temperature += delta / 8
		elif has_pants and !has_shirt:
			modulate = Color(Color.WHITE, 255)
			temperature -= delta / 8
		elif !has_pants and !has_shirt:
			modulate = Color(Color.WHITE, 255)
			temperature -= delta / 5			
				 						
	elif Global.temperature >= 35:
		if sweating:
			modulate = Color(Color.WHITE, 255)
			if has_pants and has_shirt:
				temperature += delta / 8
			if has_pants and !has_shirt:
				temperature += delta / 10
			else:
				temperature += delta / 12
		elif $"../Body".rapid_heart_rate:
			modulate = Color(Color.RED, 65)
			if has_pants and has_shirt:
				temperature += delta / 8
			elif has_pants and !has_shirt:
				temperature += delta / 9
			else:
				temperature += delta / 10	
			dead = heart_attack()
				
		elif has_pants and has_shirt:
			modulate = Color(Color.WHITE, 255)
			temperature += delta / 5
		elif has_pants and !has_shirt:
			modulate = Color(Color.WHITE, 255)
			temperature += delta / 6
		elif !has_pants and !has_shirt:
			modulate = Color(Color.WHITE, 255)
			temperature += delta / 7						
	elif Global.temperature < 30:
		if !has_pants and !has_shirt:
			temperature -= delta / 5
		elif has_pants and !has_shirt:
			temperature -= delta / 7		
	
	
	if temperature < 38:
		billy_state = state.HAPPY
	elif  temperature >= 38 && temperature < 40:
		billy_state = state.NORMAL
	elif  temperature >= 40 && temperature <= 41:
		billy_state = state.EXHAUSTED
	elif  temperature > 41:
		dead = true				
	
	
	if has_pants and has_shirt:
		if billy_state == state.HAPPY:
			texture_normal = happy_billy[0]
			texture_pressed = happy_billy[0]
		elif billy_state == state.NORMAL:
			if sweating:
				texture_normal = normal_billy[1]
				texture_pressed = normal_billy[1]
			else:
				texture_normal = normal_billy[0]
				texture_pressed = normal_billy[0]
		else:
			if sweating:
				texture_normal = sad_billy[1]
				texture_pressed = sad_billy[1]
			else:
				texture_normal = sad_billy[0]
				texture_pressed = sad_billy[0]
	elif has_pants and !has_shirt:
		if billy_state == state.HAPPY:
			texture_normal = happy_billy[1]
			texture_pressed = happy_billy[1]
		elif billy_state == state.NORMAL:
			texture_normal = normal_billy[2]
			texture_pressed = normal_billy[2]
		else:
			texture_normal = sad_billy[2]
			texture_pressed = sad_billy[2]
	elif !has_pants and !has_shirt:
		if billy_state == state.HAPPY:
			texture_normal = happy_billy[2]
			texture_pressed = happy_billy[2]
		elif billy_state == state.NORMAL:
			texture_normal = normal_billy[3]
			texture_pressed = normal_billy[3]
		else:
			texture_normal = sad_billy[3]
			texture_pressed = sad_billy[3]						

func get_shirt() -> bool:
	return has_shirt
func set_shirt(shirt):
	has_shirt = shirt

func heart_attack() -> bool:
	var random_number = randi() % 6
	var attack = randi() % 6
	
	print_debug(str(random_number) + ":" + str(attack))
	
	if random_number == attack:
		return true
	else:
		return false 
	

func get_pants() -> bool:
	return has_pants
func set_pants(pants):
	has_pants = pants	
