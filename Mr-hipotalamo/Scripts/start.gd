extends Control

var texts = ["Summer arrived", "Intense temperatures and heat waves are starting to show", "We cannot let Billy die", "Survive two months"]

var index = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	$Label.text = texts[index]
	$AnimationPlayer.play("write")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
	
	


func _on_animation_player_animation_finished(anim_name):
	if index < 3:
		index += 1
		$Label.text = texts[index]
		$AnimationPlayer.play("write")
	else:
		get_parent().start_game()	
