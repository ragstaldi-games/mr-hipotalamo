extends ColorRect

const MINUTES_PER_DAY = 1440
const MINUTES_PER_HOUR = 60
const INGAME_TO_REAL_MINUTE_DURATION = (2*PI) / MINUTES_PER_DAY
const INGAME_SPEED = 5.0
const INITIAL_TIME_HOUR = 10

@export var gradient : GradientTexture1D

var time = 0.0
var past_minute = -1.0


# Called when the node enters the scene tree for the first time.
func _ready():
	time = INGAME_TO_REAL_MINUTE_DURATION * INITIAL_TIME_HOUR * MINUTES_PER_HOUR


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	time += delta * INGAME_TO_REAL_MINUTE_DURATION * INGAME_SPEED
	var value = (sin(time - PI/2) + 1.0) / 2.0
	color = gradient.gradient.sample(value)
	
	recalculate_time()

func recalculate_time():
	var total_minutes = int(time / INGAME_TO_REAL_MINUTE_DURATION)
	
	var day = int(total_minutes / MINUTES_PER_DAY)
	
	var current_day_minutes = total_minutes % MINUTES_PER_DAY
	
	var hour = int(current_day_minutes / MINUTES_PER_HOUR)
	var minutes = int(current_day_minutes % MINUTES_PER_HOUR)
	
	if past_minute != minutes:
		past_minute = minutes
		Global.set_day_time(day,hour,minutes)
	
