extends TouchScreenButton

var rapid_heart_rate = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func set_heart_rate():
	if rapid_heart_rate:
		rapid_heart_rate = false
	else:
		rapid_heart_rate = true
		
func sweat():
	if 	$"../Billy".sweating:
		$"../Billy".sweating = false
	else:
		$"../Billy".sweating = true			
