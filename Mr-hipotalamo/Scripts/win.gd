extends Control

var texts = ["Autumm arrived, we survived the heat", "Now Billy can live calmly", "Good job Mr. Hypothalamus, see you around winter"]
var index = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	$Label.text = texts[index]
	$AnimationPlayer.play("write")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_animation_player_animation_finished(anim_name):
	if index <= 2:
		index += 1
		$Label.text = texts[index]
		$AnimationPlayer.play("write")
	else:
		get_tree().quit()	
