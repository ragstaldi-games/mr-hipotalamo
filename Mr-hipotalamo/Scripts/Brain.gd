extends Control
enum action{
	DEFAULT,
	DRESSING,
	BODY
}

var undress = false

var current_action : action = action.DEFAULT

var ac_on = false

var is_billy = true

# Called when the node enters the scene tree for the first time.
func _ready():
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$Termometro/ProgressBar.value = Global.temperature	
	if Global.minute < 10 and Global.hour < 10:
		$Clock/Label.text = "0" + str(Global.hour) + ":0" + str(Global.minute)
	elif Global.minute >= 10 and Global.hour < 10:
		$Clock/Label.text = "0" + str(Global.hour) + ":" + str(Global.minute)
	elif Global.minute < 10 and Global.hour >= 10:
		$Clock/Label.text = str(Global.hour) + ":0" + str(Global.minute)	
	else:
		$Clock/Label.text = str(Global.hour) + ":" + str(Global.minute)	
	
	if Global.hour > 10 && Global.hour < 16 && !ac_on:
		Global.temperature += delta / 2
	elif ac_on and Global.temperature > 25:
		Global.temperature -= delta / 2	
	elif (Global.hour > 18 || Global.hour <= 10) && Global.temperature > 25:
		Global.temperature -= delta / 10	
	
	if $Billy.dead:
		get_parent().you_loose_scene()
		
	if Global.day >= 60:
		get_parent().win()
		
	if ac_on:
		if Global.power > 0:
			Global.power -= delta / 10
		else:
			ac_on = false
			$AC/Light.color = Color.RED
	else:
		if Global.power < 100:
			Global.power += delta / 20		 		


func set_billys_clothes():
	if $Billy.get_shirt() && $Billy.get_pants():
		$Button/Label.text = "Remove shirt"
		$Button2/Label.text = "Remove pants"
		current_action = action.DRESSING
	elif !$Billy.get_shirt() && $Billy.get_pants():
		$Button/Label.text = "Put shirt"
		$Button2/Label.text = "Remove pants"
		current_action = action.DRESSING
	elif !$Billy.get_shirt() && !$Billy.get_pants():
		$Button/Label.text = "Put shirt"
		$Button2/Label.text = "Put pants"
		current_action = action.DRESSING

func set_body_conditions():
	if 	!$Body.rapid_heart_rate and !$Billy.sweating:
		$Button/Label.text = "Increase heart rate"
		$Button2/Label.text = "Start sweating"
	elif $Body.rapid_heart_rate and !$Billy.sweating:
		$Button/Label.text = "Reduce heart rate"
		$Button2/Label.text = "Start sweating"
	elif $Body.rapid_heart_rate and $Billy.sweating:
		$Button/Label.text = "Reduce heart rate"
		$Button2/Label.text = "Stop sweating"
	elif !$Body.rapid_heart_rate and $Billy.sweating:
		$Button/Label.text = "Increase heart rate"
		$Button2/Label.text = "Stop sweating"		
	
	current_action = action.BODY
					 

func _on_billy_pressed():
	set_billys_clothes()		
	$Button.show()
	$Button2.show()
	
func _on_button_pressed():
	if current_action == action.DRESSING:
		$Comms.text = ""
		if $Billy.get_shirt():
			$Billy.set_shirt(false)
			set_billys_clothes()
		else :
			if $Billy.get_pants():
				$Billy.set_shirt(true)
				set_billys_clothes()
			else :
				$Comms.text = "Put the pants first"	
				$AnimationPlayer.play("write")
	if current_action == action.BODY:
		$Comms.text = "Keep in mind this quick heart rate, when the heat strats hitting, Billy might collapse of a heart attack"
		$AnimationPlayer.play("write")
		$Body.set_heart_rate()
		set_body_conditions()
						


func _on_button_2_pressed():
	if current_action == action.DRESSING:
		if $Billy.get_shirt():
			$Comms.text = "We must take out the shirt first"
			$AnimationPlayer.play("write")
		else :
			$Comms.text = ""
			if $Billy.get_pants():
				$Billy.set_pants(false)
				set_billys_clothes()
			else :	
				$Billy.set_pants(true)
				set_billys_clothes()
				
	if current_action == action.BODY:
		if Global.temperature < 30:
			$Comms.text = "It´s a bit cold to sweat, we will get sick"
			$AnimationPlayer.play("write")
		else:	
			$Body.sweat()
			set_body_conditions()			


func _on_ac_pressed():
	if ac_on:
		ac_on = false
		$AC/Light.color = Color.RED
	else:
		ac_on = true
		$AC/Light.color = Color.GREEN	


func _on_swap_button_pressed():
	if is_billy:
		is_billy = false
		$Billy.hide()
		$Body.show()
		if $Body.rapid_heart_rate:
			$AnimationPlayer.play("increased_beat")
		else:	
			$AnimationPlayer.play("beat")
		$Button.hide()
		$Button2.hide()
	else:
		is_billy = true
		$Body.hide()
		$Billy.show()
		$Button.hide()
		$Button2.hide()
		
			


func _on_body_pressed():
	$Button.show()
	$Button2.show()
	set_body_conditions()


func _on_animation_player_animation_finished(anim_name):
	if anim_name == "write":
		if $Body.rapid_heart_rate:
			$AnimationPlayer.play("increased_beat")
		else:
			$AnimationPlayer.play("beat")	
