extends Node

var ui = preload("res://Nodes/Brain.tscn")
var loose = preload("res://Nodes/loose.tscn")
var start = preload("res://Nodes/start.tscn")
var _win = preload("res://Nodes/win.tscn")

var instance

# Called when the node enters the scene tree for the first time.
func _ready():
	first()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func first():
	instance = start.instantiate()
	add_child(instance)

func start_game():
	Global.temperature = 25
	Global.day = 0
	Global.hour = 0
	Global.minute = 0
	
	remove_child(instance)
	instance = ui.instantiate()
	add_child(instance)

func win():
	remove_child(instance)
	instance = _win.instantiate()
	add_child(instance)

func you_loose_scene():
	remove_child(instance)
	instance = loose.instantiate()
	add_child(instance)
